<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_classes', 'Configuration/TypoScript', 'hive_ext_classes');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextclasses_domain_model_abstractarticle', 'EXT:hive_ext_classes/Resources/Private/Language/locallang_csh_tx_hiveextclasses_domain_model_abstractarticle.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextclasses_domain_model_abstractarticle');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextclasses_domain_model_article', 'EXT:hive_ext_classes/Resources/Private/Language/locallang_csh_tx_hiveextclasses_domain_model_article.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextclasses_domain_model_article');

    }
);
