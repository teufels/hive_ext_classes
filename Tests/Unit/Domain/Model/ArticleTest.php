<?php
namespace HIVE\HiveExtClasses\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class ArticleTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtClasses\Domain\Model\Article
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtClasses\Domain\Model\Article();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
